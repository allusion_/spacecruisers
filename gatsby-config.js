require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
  siteMetadata: {
    title: "Spacecruisers",
    titleTemplate: "%s - Spacecruisers",
    description:
      "Our team adds up of ambitious young adults, developing several projects and trying to bring more innovation into the world of NFTs.",
    url: "https://www.spacecruisers.io", // No trailing slash allowed!
    image: "/cover_preload-tinified.png", // Path to the image placed in the 'static' folder, in the project's root directory.
    twitterUsername: "@tspacecruisers",
  },
  plugins: [
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        // You can add multiple tracking ids and a pageview event will be fired for all of them.
        trackingIds: [
          "G-GNX8EXLH0M", // Google Analytics / GA
        ],
        // This object is used for configuration specific to this plugin
        pluginConfig: {
          // Puts tracking script in the head instead of the body
          head: true,
          // Setting this parameter is also optional
          respectDNT: true,
          // Avoids sending pageview hits from custom paths
          exclude: ["/preview/**", "/do-not-track/me/too/"],
        },
      },
    },
    {
      resolve: `gatsby-plugin-gdpr-cookies`,
      options: {
        googleAnalytics: {
          trackingId: "G-GNX8EXLH0M", // leave empty if you want to disable the tracker
          cookieName: "gatsby-gdpr-google-analytics", // default
          anonymize: true, // default
          allowAdFeatures: false, // default
        },
      },
    },
    `gatsby-plugin-preact`,
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [`Dosis:400,500`],
        display: "swap",
      },
    },
    {
      resolve: "gatsby-source-contentful",
      options: {
        accessToken: "O2VvLTZqglnCGflFDcu8dPR3FfJjW5U5aan8YexvMT4",
        spaceId: "avbti9ktpdj6",
      },
    },
    "gatsby-plugin-styled-components",
    "gatsby-plugin-image",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/assets/",
      },
      __key: "images",
    },
    {
      resolve: "gatsby-plugin-anchor-links",
      options: {
        offset: -200,
      },
    },
  ],
};
