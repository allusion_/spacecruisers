import styled from "styled-components";

const MaxWidthContainerWidth = 1350;
const ContentContainerWidth = 1360;

const FullWidthContainer = styled("div")({
  height: "100%",
  minHeight: 600,
  backgroundColor: "transparent",
  "@media print": {
    backgroundColor: "transparent",
  },
});

const MaxWidthContainer = styled("div")({
  maxWidth: MaxWidthContainerWidth,
  margin: "0 auto",
  position: "relative",
  padding: 20,
});

const ContentContainer = styled("div")({
  maxWidth: ContentContainerWidth,
  margin: "0 auto",
  position: "relative",
  padding: 20,
});

export { FullWidthContainer, MaxWidthContainer, ContentContainer };
