const colors = {
  dark: "#000912",
  light: "#ffffff",
  blueish: "#232438",
  purpleish: "#20182c",
  blue: "#04c9e5",
  purple: "#8B50E5",
};

const sizes = {
  headerHeight: "130px",
  headerHeightScroll: "180px",
};

const constrains = {
  mainPlanetTop: "700px",
};

export { colors, sizes, constrains };
