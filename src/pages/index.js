import React from "react";
import styled, { keyframes, css } from "styled-components";
import { useState, useEffect } from "react";
import { createClient } from "contentful";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { ContentContainer } from "../styling/layout";
import Layout from "../components/Layout/Layout";
import Lottie from "react-lottie-player";

import Hero from "../components/Hero/Hero";
import Section from "../components/Section/Section";
import Video from "../components/Video/Video";
import Roadmap from "../components/Roadmap/Roadmap";
import Faq from "../components/FAQ/Faq";
import Footer from "../components/Footer/Footer";

import SEO from "../components/SEO/Seo";

import PlanetTop from "../assets/Planet_top.png";
import PlanetBottom from "../assets/planet_bottom.svg";

import Crater from "../components/Planets/Crater";
import ShadowLine from "../components/Planets/ShadowLine";

import Lab from "../components/Planets/Lab";
import Tehen1 from "../assets/animarions/Tehen.json";

import PrivacyPolicy from "../components/PrivacyPolicy/PrivacyPolicy";
import Cookies from "../components/Cookies/Cookies";

import { useMediaQuery } from "react-responsive";
import mq from "../styling/mediaQueries";

const topWidth = 2576;
const topHeight = 600;
const middleWidth = 2000;
const middleHeight = 2570;
const bottomWidth = 2576;
const bottomHeight = 600;

const Pulse = keyframes`
  0% {
    background-color: #7f0a91;
  }
  50% {
    background-color: #cb2ce3;
  }
  100 {
    background-color: #7f0a91;
  }
`;

const SectionWrapper = styled("div")(
  {
    position: "relative",
  },
  mq({
    padding: ["50px 0 0 0", "50px 0 0 0", "150px 0 0 0"],
  })
);

const client = createClient({
  // This is the space ID. A space is like a project folder in Contentful terms
  // eslint-disable-next-line no-undef
  space: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE_ID,
  // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
  // eslint-disable-next-line no-undef
  accessToken: process.env.NEXT_PUBLIC_CONTENTFUL_ACCESS_KEY,
});

const LabWrapper = styled("div")({
  position: "absolute",
  top: "30%",
  right: "20%",
});

const AnimationWrapper = styled("div")(
  {
    transform: "rotate(10deg)",
    position: "absolute",
    zIndex: 1,
  },
  mq({
    width: [300, 300, 800],
    top: ["0", "0", "14%"],
    left: ["-20%", "-*20%", "-10%"],
  })
);

export default function Home() {
  const [intro, setIntro] = useState();
  const [aboutUs, setAboutUs] = useState();
  const [cruiseville, setCruiseville] = useState();
  const [faq, setFaq] = useState();
  const [roadmap, setRoadmap] = useState();
  const [privacy, setPrivacy] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1025px)" });
  const [modalOpen, setModalOpen] = useState();

  const SectionBackground = styled("div")({
    // width: "100%",
    // height: "auto",
    // position: "absolute",

    "&.top": {
      background: `url("${PlanetTop}") top center no-repeat`,
      // backgroundRepeat: "no-repeat",
      // backgroundPosition: "center",
      // backgroundSize: "contain",

      // marginTop: "-200px",
      height: `${topHeight}px`,
      marginBottom: "-60px",
    },
    "&.middle": {
      // background: `url("${PlanetMid}") top center repeat`,
      // background:
      //   "radial-gradient(circle, rgba(21,108,152,1) 0%, rgba(24,107,152,1) 100%)",
      background:
        "linear-gradient(180deg, rgba(24,107,152,1) 0%, #0A5F79 100%)",
      // height: `${middleHeight}px`,
      height: "90%",
    },
    "&.bottom": {
      position: "relative",
      background: `url("${PlanetBottom}") top center no-repeat`,
      height: `${bottomHeight}px`,
      marginTop: !isTabletOrMobile ? "-100px" : "0",
      ".crater-background": {
        position: "absolute",
        background: "#7f0a91",
        animation: css`
          ${Pulse} 10s ease infinite
        `,
        width: "20%",
        height: 150,
        marginTop: 100,
        left: "5%",
        transform: "rotate(10deg)",
        zIndex: -1,
        display: !isTabletOrMobile ? "block" : "none",
      },
    },
  });

  useEffect(() => {
    // client
    //   .getEntry("2SHBWcRXYLq4iepnl7ngN1")
    //   .then((entry) => {
    //     setIntro(entry);
    //     setIsLoading(false);
    //   })
    //   .catch((err) => console.log(err));

    client.getEntries().then(function (entries) {
      // log the title for all the entries that have it
      entries.items.forEach(function (entry) {
        // if (entry.sys.id === "2SHBWcRXYLq4iepnl7ngN1") {
        //   setIntro(entry);
        // }
        switch (entry.sys.id) {
          case "2SHBWcRXYLq4iepnl7ngN1":
            setIntro(entry);
            break;
          case "1mDhIliMcV6sBpUzIYSlc6":
            setAboutUs(entry);
            break;
          case "1hSb0nXEe5tlQWZICBLbMh":
            setCruiseville(entry);
            break;
        }
      });
      setIsLoading(false);
    });

    client
      .getEntries({
        content_type: "faq",
      })
      .then((response) => {
        setFaq(response.items);
        console.log(response.items);
      });

    client
      .getEntries({
        content_type: "roadmap",
      })
      .then((response) => {
        setRoadmap(response.items);
      });

    client.getEntry("1xbsct9m70mFFbSRTBe6oT").then((response) => {
      setPrivacy(response);
    });
  }, []);

  useState(() => {
    const url = typeof window !== "undefined" ? window.location.href : "";
    console.log(url);
    if (url.indexOf("#privacy") != -1) {
      setModalOpen(true);
    }
  }, [modalOpen]);

  return (
    <>
      <SEO
        title={`Welcome`}
        description={`Our team adds up of ambitious young adults, developing several projects and trying to bring more innovation into the world of NFTs.`}
        image={`/cover.png`}
        pathname={`/`}
      />
      <Layout>
        <Cookies setPolicy={setModalOpen} />
        <PrivacyPolicy
          data={privacy}
          setModalOpen={setModalOpen}
          isOpenModal={modalOpen}
        />
        <Hero isLoading={isLoading} />
        <>
          {/* <SectionBackground className="top" /> */}
          <SectionBackground className="middle">
            <SectionWrapper>
              <div id="introduction">
                <Section
                  sectionName="intro"
                  title={intro?.fields?.sectionTitle}
                  text={documentToReactComponents(
                    intro?.fields?.sectionContent
                  )}
                />
              </div>
            </SectionWrapper>
            <SectionWrapper>
              <Crater
                size={!isTabletOrMobile ? 2 : 0.8}
                rotation={-19}
                position={{ top: 150, left: 120 }}
              />
              <ShadowLine
                rotation={5}
                size={1.5}
                position={{ top: 550, left: -400 }}
              />
              <div id="about-us">
                <Section
                  sectionName="about-us"
                  title={aboutUs?.fields?.sectionTitle}
                  text={documentToReactComponents(
                    aboutUs?.fields?.sectionContent
                  )}
                />
              </div>
              {!isTabletOrMobile && (
                <Crater
                  size={1}
                  rotation={25}
                  position={{ top: 50, right: 50 }}
                />
              )}
            </SectionWrapper>
            <SectionWrapper>
              <Video />
            </SectionWrapper>

            <SectionWrapper>
              <div id="cruiseville">
                <LabWrapper>
                  <Lab />
                </LabWrapper>
                <Section
                  sectionName="cruiseville"
                  title={cruiseville?.fields?.sectionTitle}
                  text={documentToReactComponents(
                    cruiseville?.fields?.sectionContent
                  )}
                />
              </div>
              {/* <CraterMoo size={1.5} position={{ top: 150, left: 120 }} /> */}
            </SectionWrapper>
          </SectionBackground>

          <SectionBackground className="bottom">
            <div className="crater-background" />
            <AnimationWrapper>
              <Lottie
                rendererSettings={{ preserveAspectRatio: "xMidYMid slice" }}
                loop
                animationData={Tehen1}
                play
              />
            </AnimationWrapper>
          </SectionBackground>
          <div id="roadmap">
            <Roadmap roadmap={roadmap} />
          </div>
          <div id="faq">
            <Faq faqElements={faq} />
            <Footer />
          </div>
        </>
      </Layout>
    </>
  );
}
