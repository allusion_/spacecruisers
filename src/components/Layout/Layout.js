import React from "react";
import PropTypes from "prop-types";
import { LayoutWrapper, GlobalStyle } from "./Layout.styled";

import MovingStars from "../Backgrounds/MovingStars";
import MainPlanet from "../Planets/MainPlanet";
import Header from "../Header/Header";
// import Footer from "../Footer/Footer";

const Layout = ({ children }) => {
  return (
    <LayoutWrapper>
      <Header />
      <main>
        {children}
        {/* <MainPlanet /> */}
      </main>
      {/* <Footer /> */}
      <MovingStars />
      <GlobalStyle />
    </LayoutWrapper>
  );
};

Layout.propTypes = {
  children: PropTypes.any,
};

export default Layout;
