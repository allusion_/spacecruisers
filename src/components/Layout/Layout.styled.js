import styled, { createGlobalStyle, keyframes, css } from "styled-components";
import { colors } from "../../styling/variables";
import mq from "../../styling/mediaQueries";

const AnimateBackground = keyframes`
  0%{background-position:22% 0%}
  50%{background-position:79% 100%}
  100%{background-position:22% 0%}
`;

export const GlobalStyle = createGlobalStyle({
  html: {
    scrollBehaviour: "smooth",
  },
  body: {
    fontSize: "30px",
    lineHeight: "42px",
    margin: 0,
    padding: 0,
    overflowX: "hidden",
    fontFamily: "'Dosis', sans-serif",
    // background: colors.dark,
    background: `linear-gradient(151deg, ${colors.dark}, ${colors.purpleish})`,
    backgroundSize: "400% 400%",
    animation: css`
      ${AnimateBackground} 10s ease infinite
    `,
    color: colors.light,
    scrollBehaviour: "smooth",
  },
  a: {
    textDecoration: "none",
    opacity: 1,
    transition: "all 1s",
    ":hover": {
      opacity: "0.7",
    },
  },
  h2: {
    fontSize: "52px",
    fontWeight: "normal",
  },
  // section: {
  //   margin: "80px 0",
  // },
});

export const LayoutWrapper = styled("div")(
  {
    margin: "0",
  },
  mq({
    width: ["100vw", "100vw", "auto"],
    overflow: ["hidden", "hidden", "hidden"],
  })
);

export default GlobalStyle;
