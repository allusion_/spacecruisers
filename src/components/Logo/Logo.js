import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { StaticImage } from "gatsby-plugin-image";
// import LogoImage from "../../assets/spacecruisers_logo.svg";

const LogoWrapper = styled("div")({
  width: 93,
  height: 93,
  transition: "all 0.8s",
  img: {
    width: 93,
    height: 93,
    transition: "all 0.8s",
  },
  ".smaller": {
    transform: "scale(0.7)",
  },
});

const Logo = ({ isSmaller }) => {
  return (
    <LogoWrapper>
      <StaticImage
        className={isSmaller ? "smaller" : ""}
        src="../../assets/logo2.svg"
        alt="Spacecruisers"
        placeholder="blurred"
        layout="fullWidth"
        style={{
          transition: "all 0.8s",
        }}
      />
    </LogoWrapper>
  );
};

export default Logo;

Logo.propTypes = {
  isSmaller: PropTypes.bool,
};
