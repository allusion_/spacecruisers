import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { MaxWidthContainer } from "../../styling/layout";
import mq from "../../styling/mediaQueries";

const FooterWrapper = styled("div")(
  {
    color: "white",
    width: "100%",
    background: "black",
    a: {
      color: "white",
    },
    ".separator": {
      padding: "0 15px",
    },
  },
  mq({
    fontSize: [12, 12, 18],
  })
);

const Footer = () => {
  const [year, setYear] = useState();

  useEffect(() => {
    const actualYear = new Date().getFullYear();
    setYear(actualYear);
  }, []);

  return (
    <FooterWrapper>
      <MaxWidthContainer>
        © Copyright {year} The Spacecruisers. All Rights Reserved{" "}
        <span className="separator">I</span>
        <a href="/Whitepaper.pdf" target="_blank">
          Whitepaper
        </a>
      </MaxWidthContainer>
    </FooterWrapper>
  );
};

export default Footer;
