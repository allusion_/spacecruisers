import React, { useState } from "react";
import styled from "styled-components";
import { MaxWidthContainer } from "../../styling/layout";
import { StaticImage } from "gatsby-plugin-image";
import Preload from "../../assets/cover_preload-tinified.png";
import { GrCirclePlay } from "react-icons/gr";
import { colors } from "../../styling/variables";
import Button from "../Button/Button";
import mq from "../../styling/mediaQueries";

const VideoWrapper = styled("div")({
  position: "relative",
});

const VideoHolder = styled("div")(
  {
    position: "absolute",
    width: "88%",
    height: "81%",
    background: `url(${Preload})`,
    backgroundSize: "cover",
    zIndex: 1,
    top: 18,
    left: "95px",
    overflow: "hidden",
  },
  mq({
    top: ["5px", "5px", "18px"],
    left: ["25px", "25px", "95px"],
  })
);

const PlayButtonWrapper = styled("div")(
  {
    position: "absolute",
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "white",
    ".icon": {
      cursor: "pointer",
      transform: "rotate(0deg) scale(1)",
      transition: "all 1s",
      filter: "drop-shadow( 3px 3px 2px rgba(0, 0, 0, .4))",
      "&:hover": {
        transform: "rotate(360deg) scale(1.3)",
        filter: "drop-shadow( 3px 3px 12px rgba(0, 0, 0, .4))",
      },
    },
    svg: {
      path: {
        stroke: "white",
        transition: "all 1s",
      },
      "&:hover": {
        path: {
          stroke: colors.blue,
        },
      },
    },
  },
  mq({
    svg: {
      width: ["62px", "62px", "auto"],
    },
  })
);

const YoutubeVideo = styled("div")({
  position: "absolute",
  width: "88%",
  height: "81%",
  zIndex: 1,
  top: 18,
  left: "95px",
});

const ButtonWrapper = styled("div")(
  {
    margin: "0 0 15px 0",
    textAlign: "center",
  },
  mq({
    marginTop: ["30px", "30px", "auto"],
  })
);

const Video = () => {
  const [isVideoPlaying, setIsVideoPlaying] = useState(false);

  const playVideo = () => {
    setIsVideoPlaying(true);
  };

  return (
    <MaxWidthContainer>
      <VideoWrapper>
        {isVideoPlaying && (
          <YoutubeVideo>
            <iframe
              width="100%"
              height="100%"
              src="https://www.youtube.com/embed/dQw4w9WgXcQ?autoplay=1"
              title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            ></iframe>
          </YoutubeVideo>
        )}
        {!isVideoPlaying && (
          <VideoHolder>
            <PlayButtonWrapper>
              {/* <GrCirclePlay onClick={playVideo} className="icon" size={200} /> */}
            </PlayButtonWrapper>
          </VideoHolder>
        )}
        <StaticImage
          src="../../assets/video_holder.png"
          alt="Spacecruisers"
          placeholder="blurred"
          layout="fullWidth"
          style={{
            transition: "all 0.8s",
          }}
        />
      </VideoWrapper>
      <ButtonWrapper>
        <Button
          link="https://linktr.ee/thespacecruisers"
          text="Explore NFT’s"
        />
      </ButtonWrapper>
    </MaxWidthContainer>
  );
};

export default Video;
