import React from "react";
import PropTypes from "prop-types";
import styled, { keyframes, css } from "styled-components";
import { colors } from "../../styling/variables";
import Gradient from "rgt";

import CharityPlanetTexture from "../../assets/planet_charity_texture.svg";

const Pulsing = keyframes`
  0% { box-shadow: 0 0 45px 50px #3781d133; }
	50% { box-shadow: 0 0 45px 50px rgba(63, 134, 200, 0.3); }
	100% { box-shadow: 0 0 45px 50px #218bd133; }
`;

const Rotate = keyframes`
  100% { transform:rotate(-360deg); transition: transform 20s; }
`;

const CharityWrapper = styled("div")({
  position: "relative",
  textAlign: "center",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  width: 500,
  height: 500,
  top: 130,
  zIndex: 1,
});

const CharityBackground = styled("div")({
  width: 500,
  height: 500,
  borderRadius: "100%",
  background:
    "radial-gradient(circle, rgba(0,116,119,1) 0%, rgba(55,56,107,1) 100%)",
  position: "absolute",
  top: "0",
  left: "0",
  zIndex: -1,
  animation: css`
    ${Rotate} 200s infinite linear both
  `,
});

const Title = styled("h3")({
  fontSize: 48,
  color: "white",
  fontWeight: "normal",
  margin: 0,
});

const Phase = styled("div")({
  fontSize: 30,
  color: "white",
});

const Description = styled("div")({
  fontSize: 34,
  color: "white",
  padding: "10px 30px",
});

const CharityTexture = styled("div")({
  width: 500,
  height: 500,
  position: "absolute",
  backgroundImage: `url(${CharityPlanetTexture}) `,
  backgroundRepeat: "no-repeat",
  backgroundSize: "500px 500px",
  top: "0",
  left: "0",
  mixBlendMode: "multiply",
  animation: css`
    ${Rotate} 200s infinite linear both
  `,
  // zIndex: -1,
});

const CharityPlanetPulsating = styled("div")({
  width: 500,
  height: 500,
  position: "absolute",
  borderRadius: "100%",
  zIndex: -3,
  top: 0,
  left: 0,
  animation: css`
    ${Pulsing} 10s ease infinite
  `,
});

const CharityElement = ({ id, title, description }) => {
  return (
    <CharityWrapper>
      <Phase>Phase {id}</Phase>
      <Title>
        <Gradient dir="left-to-right" from={colors.blue} to={colors.purple}>
          {title}
        </Gradient>
      </Title>
      <Description>{description}</Description>
      <div>
        <CharityTexture />
        <CharityBackground />
        <CharityPlanetPulsating />
      </div>
    </CharityWrapper>
  );
};

export default CharityElement;

CharityElement.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string,
  description: PropTypes.string,
};
