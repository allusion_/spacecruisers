import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { MaxWidthContainer } from "../../styling/layout";
import Lottie from "react-lottie-player";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";

import AnimBase from "../../assets/animarions/Roadmap.json";
import RoadmapElement from "./RoadmapElement";
import CharityElement from "./CharityElement";
import Astronaut from "../Planets/Astronaut";
import Meteor from "../Planets/Meteor";
import Saturn from "../Planets/Saturn";
import Clouds from "../Planets/Clouds";

import { useMediaQuery } from "react-responsive";
import mq from "../../styling/mediaQueries";

const RoadmapWrapper = styled("div")(
  {
    position: "relative",
    display: "flex",
    justifyContent: "center",

    zIndex: -1,
  },
  mq({
    flexDirection: ["column", "column", "row"],
    // height: ["100vh", "100vh", "auto"],
    marginTop: [0, 0, 250],
    marginBottom: [0, 0, 400],
  })
);

const TitleWrapper = styled("h2")({
  textAlign: "center",
  marginTop: -400,
});

const AnimationWrapper = styled("div")(
  {
    marginTop: -250,
  },
  mq({
    "> div": {
      height: ["100%", "100%", "auto"],
    },
  })
);

const CharityWrapper = styled("div")(
  {
    bottom: 0,
    left: "50%",
    marginLeft: -250,
    margni: "0 auto",
    transition: "all 1s",
  },
  mq({
    position: ["relative", "relative", "absolute"],
    marginBottom: [0, 0, -300],
    display: ["none", "none", "block"],
  })
);

const AstronautWrapper = styled("div")({
  position: "absolute",
  "&.bigger": {
    top: 100,
    right: 0,
  },
  "&.smaller": {
    top: "90%",
    left: "-20%",
    transform: "rotate(142deg)",
  },
});

const SaturnWrapper = styled("div")(
  {
    position: "absolute",
  },
  mq({
    top: ["-35%", "-35%", "-10%"],
    left: ["60%", "60%", "30%"],
  })
);

const Roadmap = ({ roadmap }) => {
  const [lastElement, setLastElement] = useState({});
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1025px)" });

  useEffect(() => {
    // const slicedArray = roadmap?.reverse().pop();
    const lastElement = roadmap?.reverse().pop();
    setLastElement(lastElement);
    // console.log(`sliced: ${slicedArray}`);
    // const lastArrayElement = slicedArray.pop();
    // setLastElement(lastArrayElement);
    console.log(`last: ${JSON.stringify(lastElement)}`);
  }, [roadmap]);

  return (
    <MaxWidthContainer>
      <TitleWrapper>Roadmap</TitleWrapper>
      <RoadmapWrapper>
        {!isTabletOrMobile && (
          <AnimationWrapper>
            <Lottie
              rendererSettings={{ preserveAspectRatio: "xMidYMid slice" }}
              loop
              animationData={AnimBase}
              play
            />
          </AnimationWrapper>
        )}
        {roadmap?.map((element) => (
          <RoadmapElement
            key={element.fields.phase}
            id={parseInt(element.fields.phase)}
            title={element.fields.title}
            description={documentToReactComponents(
              element?.fields?.description
            )}
          />
        ))}
        {isTabletOrMobile && (
          <RoadmapElement
            id={parseInt(lastElement?.fields?.phase)}
            title={lastElement?.fields?.title}
            description={documentToReactComponents(
              lastElement?.fields?.description
            )}
          />
        )}
        {!isTabletOrMobile && (
          <AstronautWrapper className="bigger">
            <Astronaut />
          </AstronautWrapper>
        )}

        {!isTabletOrMobile && (
          <CharityWrapper>
            <CharityElement
              id={parseInt(lastElement?.fields?.phase)}
              title={lastElement?.fields?.title}
              description={documentToReactComponents(
                lastElement?.fields?.description
              )}
            />
          </CharityWrapper>
        )}
        <Meteor />
        <SaturnWrapper>
          <Saturn />
        </SaturnWrapper>
        <Clouds />
        <AstronautWrapper className="smaller">
          <Astronaut size={0.5} rotateSpeed={100} />
        </AstronautWrapper>
      </RoadmapWrapper>
    </MaxWidthContainer>
  );
};

export default Roadmap;

Roadmap.propTypes = {
  roadmap: PropTypes.array,
};
