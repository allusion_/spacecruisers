import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import styled, { keyframes, css } from "styled-components";
import Gradient from "rgt";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { colors } from "../../styling/variables";
import { useMediaQuery } from "react-responsive";
import mq from "../../styling/mediaQueries";

const PulseDot = keyframes`
   0% {
    transform: scale(.8);
  }
  50% {
    transform: scale(1);
  }
  100% {
    transform: scale(.8);
  }
`;

const RoadmapElementWrapper = styled("div")(
  {
    zIndex: 2,
    transition: "all 0.5s",

    "&.left": {
      // left: 0,
      textAlign: "right",
      paddingRight: "70%",
    },
    "&.right": {
      paddingLeft: "70%",
      // right: 0,
    },
    "&.active": {
      ".line": {
        "&.left": {
          "&:after": {
            width: 46,
            height: 46,
            marginTop: -23,
            marginLeft: -23,
            background:
              "linear-gradient(45deg, rgba(4,201,229,1) 0%, rgba(139,80,229,1) 100%)",
          },
        },
        "&.right": {
          "&:before": {
            width: 46,
            height: 46,
            marginTop: -23,
            marginLeft: -23,
            background:
              "linear-gradient(45deg, rgba(4,201,229,1) 0%, rgba(139,80,229,1) 100%)",
          },
        },
      },
    },
  },

  mq({
    padding: [15, 15, 0],
    margin: ["15px 0", "15px 0", 0],
    position: ["relative", "relative", "absolute"],
    background: [
      "radial-gradient(circle, rgba(0,0,0,0.7) 25%, rgba(148,187,233,0) 100%)",
      "radial-gradient(circle, rgba(0,0,0,0.7) 25%, rgba(148,187,233,0) 100%)",
      "transparent",
    ],
    width: ["100%", "100%", 500],
    maxWidth: ["100%", "100%", 500],
    "&.left": {
      // left: 0,
      textAlign: "right",
      paddingRight: [0, 0, "70%"],
    },
    "&.right": {
      paddingLeft: [0, 0, "70%"],
      // right: 0,
    },
    "&.roadmap-1": {
      top: ["0", "0", "14%"],
    },
    "&.roadmap-2": {
      top: ["20%", "20%", "24%"],
    },
    "&.roadmap-3": {
      top: ["45%", "45%", "36%"],
    },
    "&.roadmap-4": {
      top: ["60%", "60%", "48%"],
    },
    "&.roadmap-5": {
      top: ["80%", "80%", "60%"],
    },
    "&.roadmap-6": {
      top: ["98%", "98%", "72%"],
    },
    "&.roadmap-7": {
      top: ["110%", "110%", "86%"],
    },
  })
);

const Title = styled("h3")(
  {
    color: "white",
    fontWeight: "normal",
    margin: 0,
  },
  mq({
    fontSize: [32, 32, 48],
    lineHeight: ["34px", "34px", "52px"],
  })
);

const Phase = styled("div")(
  {
    color: "white",
  },
  mq({
    fontSize: [22, 22, 30],
  })
);

const Description = styled("div")(
  {
    color: "white",
  },
  mq({
    fontSize: [24, 24, 34],
    lineHeight: ["26px", "26px", "38px"],
  })
);

const DotLine = styled("div")(
  {
    position: "absolute",
    width: 300,
    height: 1,
    background: "white",
    "&.line-1": {
      width: 450,
      "&.right": {
        left: -360,
      },
    },
    "&.line-2": {
      width: 310,
      "&.right": {
        left: -380,
      },
    },
    "&.line-3": {
      width: 310,
      "&.left": {
        left: 340,
      },
    },
    "&.line-4": {
      width: 230,
      "&.left": {
        right: -140,
      },
    },
    "&.line-5": {
      width: 350,
      "&.right": {
        left: -260,
      },
    },
    "&.line-6": {
      width: 550,
      "&.left": {
        right: -460,
      },
    },
    "&.line-7": {
      width: 350,
      "&.right": {
        left: -260,
      },
    },
    "&.left": {
      right: -220,
      "&:after": {
        content: "''",
        width: "30px",
        height: "30px",
        display: "inline-block",
        borderRadius: "100%",
        background: "white",
        marginTop: -15,
        marginLeft: -15,
        position: "absolute",
        animation: css`
          ${PulseDot} 1.25s cubic-bezier(0.455, 0.03, 0.515, 0.955) -0.4s infinite
        `,
      },
    },
    "&.right": {
      left: -220,
      "&:before": {
        content: "''",
        width: "30px",
        height: "30px",
        display: "inline-block",
        borderRadius: "100%",
        background: "white",
        marginTop: -15,
        marginLeft: -15,
        position: "absolute",
        animation: css`
          ${PulseDot} 1.25s cubic-bezier(0.455, 0.03, 0.515, 0.955) -0.4s infinite
        `,
      },
    },
    "&.active": {
      "&.left": {
        "&:after": {
          width: 46,
          height: 46,
          marginTop: -23,
          marginLeft: -23,
          background:
            "linear-gradient(45deg, rgba(4,201,229,1) 0%, rgba(139,80,229,1) 100%)",
        },
      },
      "&.right": {
        "&:before": {
          width: 46,
          height: 46,
          marginTop: -23,
          marginLeft: -23,
          background:
            "linear-gradient(45deg, rgba(4,201,229,1) 0%, rgba(139,80,229,1) 100%)",
        },
      },
    },
  },

  mq({
    display: ["none", "none", "block"],
  })
);

const Contents = styled("div")({
  position: "relative",
});

const RoadmapElement = ({ id, title, description }) => {
  const ref = useRef(null);
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1025px)" });

  const addClass = () => {
    ref.current.classList.add("active");
  };

  const removeClass = () => {
    ref.current.classList.remove("active");
  };

  useEffect(() => {
    const element = ref.current;
    if (!isTabletOrMobile) {
      gsap.fromTo(
        element,
        {
          opacity: 0,
          y: "200%",
        },
        {
          opacity: 1,
          // x: -100,
          y: 0,
          scrollTrigger: {
            trigger: element.querySelector(".road-element"),
            start: "top 90%",
            end: "top 70%",
            scrub: false,

            // onEnter: () => addClass(),
            // onLeaveBack: () => removeClass(),
            // toggleClass: {
            //   className: "jwpnavbar--scrolled",
            //   targets: ".road-element",
            // },
            // onEnter: () => {
            //   element.classList.add("active");
            // },
            // onLeave: () => {
            //   element.classList.remove("active");
            // },
            // onEnterBack: () => {
            //   element.classList.add("active");
            // },
            // onLeaveBack: () => {
            //   element.classList.remove("active");
            // },
          },
        }
      );
    }
  }, []);

  return (
    <RoadmapElementWrapper
      className={
        !isTabletOrMobile
          ? `roadmap-${id} ${id % 2 == 0 ? "left" : "right"}`
          : `roadmap-${id} ${id % 2 == 0 ? "right" : "right"}`
      }
      ref={ref}
    >
      <Contents className="road-element">
        <Phase>Phase {id}</Phase>
        <DotLine
          className={`line line-${id} ${id % 2 == 0 ? "left" : "right"}`}
        />
        <Title className="title">
          <Gradient dir="left-to-right" from={colors.blue} to={colors.purple}>
            {title}
          </Gradient>
        </Title>
        <Description>{description}</Description>
      </Contents>
    </RoadmapElementWrapper>
  );
};

export default RoadmapElement;

RoadmapElement.propTypes = {
  id: PropTypes.number,
  phase: PropTypes.number,
  title: PropTypes.string,
  description: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
};
