import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Button from "../Button/Button";
import TextLink from "../Button/TextLink";
import HeroVideo from "../../assets/CoverVideo2.mp4";
import HeroVideoWebm from "../../assets/CoverVideo2.webm";
import HeroVideoMobile from "../../assets/CoverVideo2Phone.mp4";
import HeroVideoMobileWebm from "../../assets/CoverVideo2Phone.webm";
import HeroCover from "../../assets/cover_preload-tinified.webp";
import Rocks from "../../assets/cover_transparent.svg";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import mq from "../../styling/mediaQueries";

import { useMediaQuery } from "react-responsive";

const HeroWrapper = styled("div")({
  width: "100%",
  position: "relative",
  img: {
    maxWidth: "100%",
  },
});

const VideoWrapper = styled("div")(
  {
    // minWidth: 1920,
    // minHeight: 1080,
    width: "100%",
    height: "100vh",
    video: {
      position: "relative",
      zIndex: 0,
      objectFit: "fill",
      // height: "100vh",
      // width: "100vw",
      display: "flex",
      alignItems: "center",
      justifyCOntent: "center",
    },
  },
  mq({
    video: {
      width: ["auto", "auto", "100vw"],
      height: ["100vh", "100vh", "auto"],
      marginLeft: ["-100%", "-100%", "0"],
    },
  })
);

const TextOverlay = styled("div")({
  width: "100%",
  height: "100%",
  position: "absolute",
  zIndex: 2,
});

const TextOverlayContent = styled("div")(
  {
    position: "absolute",
    top: "30%",

    fontWeight: 500,
    textTransform: "uppercase",
    opacity: 0,
  },
  mq({
    left: ["0", "0", "15%"],
    textAlign: ["center", "center", "left"],
    width: ["100%", "100%", "auto"],
    h1: {
      fontSize: ["10vw", "10vw", "60px"],
    },
  })
);

const StaticWrapper = styled("div")({
  position: "absolute",
  zIndex: 5,
  width: "100%",
  height: "100%",
});

const LinkWrapper = styled("div")({
  fontSize: 14,
  margin: 15,
});

const RocksWrapper = styled("div")(
  {
    position: "absolute",
    // bottom: "-70%",
    left: 0,
    width: "100%",
    display: "block",
    img: {
      transition: "all 0.5s",
    },
  },
  mq({
    top: ["80%", "80%", "50%"],
    transform: ["scale(2.6)", "scale(2.6)", "scale(1)"],
  })
);

const Hero = ({ isLoading }) => {
  const ref = useRef(null);
  const video = useRef(null);
  const textContent = useRef(null);
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1025px)" });

  gsap.registerPlugin(ScrollTrigger);

  useEffect(() => {
    const element = ref.current;

    gsap.to(element, {
      scrollTrigger: {
        trigger: element.querySelector(".rocks"),
        start: "top 50%",
        end: "bottom 0",
        scrub: true,
      },
      transform: !isTabletOrMobile ? "scale(1.4)" : "scale(4)",
    });
  }, []);

  useEffect(() => {
    const element = video.current;
    gsap.fromTo(
      element,
      {
        transform: "scale(1)",
        // x: 0,
      },
      {
        transform: "scale(1.2)",
        // x: -1500,
        scrollTrigger: {
          trigger: element.querySelector(".video"),
          start: "top 0",
          end: "bottom 20%",
          scrub: true,
        },
      }
    );
  }, [isLoading]);

  useEffect(() => {
    setTimeout(() => {
      const element = textContent.current;
      const mainElement = ref.current;

      gsap.fromTo(
        element,
        {
          x: "-100%",
          opacity: 0,
          // x: 0,
        },
        {
          x: 0,
          opacity: 1,
          speed: 500,
        }
      );

      // gsap.fromTo(
      //   mainElement.querySelector(".rocks"),
      //   {
      //     y: "100%",
      //   },
      //   {
      //     y: 0,
      //     opacity: 1,
      //     visibility: "visible",
      //     // x: -1500,
      //   }
      // );
    }, 500);
  }, []);

  return (
    <HeroWrapper>
      <TextOverlay>
        <TextOverlayContent ref={textContent}>
          <h1>The Spacecruisers</h1>
          <div style={{ textAlign: "center" }}>
            <Button
              text="Explore NFT’s"
              link="https://linktr.ee/thespacecruisers"
            />
            <LinkWrapper>
              <TextLink
                text="Read our whitepaper"
                href="/Whitepaper.pdf"
                alt="Spacecruisers Whitepaper"
                blank
              />
            </LinkWrapper>
          </div>
        </TextOverlayContent>
      </TextOverlay>
      <VideoWrapper ref={video}>
        {!isTabletOrMobile && (
          <video
            className="video"
            autoPlay
            loop
            muted
            playsInline
            poster={HeroCover}
          >
            <source src={HeroVideoWebm} type="video/webm" />
            <source src={HeroVideo} type="video/mp4" />
          </video>
        )}
        {isTabletOrMobile && (
          <video
            className="video"
            autoPlay
            loop
            muted
            playsInline
            poster={HeroCover}
          >
            <source src={HeroVideoMobileWebm} type="video/webm" />
            <source src={HeroVideoMobile} type="video/mp4" />
          </video>
        )}
        <RocksWrapper ref={ref}>
          <div className="rocks">
            <img
              style={{ transition: "all 1s" }}
              src={Rocks}
              alt="Spacecruisers"
            />
          </div>
        </RocksWrapper>
      </VideoWrapper>
    </HeroWrapper>
  );
};

export default Hero;

Hero.propTypes = {
  isLoading: PropTypes.bool,
};
