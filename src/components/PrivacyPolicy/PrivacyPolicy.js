import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import mq from "../../styling/mediaQueries";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { GrClose } from "react-icons/gr";

const ModalWrapper = styled("div")({
  position: "fixed",
  zIndex: 98,
  width: "100%",
  height: "100%",
  background: "rgba(0,0,0,0.7)",
});

const PrivacyWrapper = styled("div")(
  {
    position: "absolute",
    borderRadius: 12,
    transform: "translate(-50%,-50%)",
    maxHeight: "80vh",
    left: "50%",
    top: "50%",
    zIndex: 99,
    padding: 30,
    background:
      "linear-gradient(180deg, rgba(63,119,132,1) 0%, rgba(55,81,129,1) 100%)",
    boxShadow:
      "rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px;",
    overflowY: "auto",
  },
  mq({
    width: ["100%", "100%", "800px"],
  })
);

const Header = styled("div")({
  display: "flex",
  justifyContent: "space-between",
});

const Title = styled("h2")({
  fontSize: 52,
});

const IconWrapper = styled("div")({
  fontSize: 32,
  color: "white",
  cursor: "pointer",
  svg: {
    fill: "white",
  },
});

const Content = styled("div")({
  fontSize: 22,
  h2: {
    fontSize: 42,
  },
  a: {
    color: "white",
  },
});

const PrivacyPolicy = ({ data, isOpenModal, setModalOpen }) => {
  const [isOpen, setIsOpen] = useState(false);

  const handleClose = () => {
    const body = document.getElementsByTagName("body");
    setIsOpen(false);
    setModalOpen(false);
    body[0].style.overflow = "auto";
  };

  useEffect(() => {
    const body = document.getElementsByTagName("body");
    if (isOpen) {
      body[0].style.overflow = "hidden";
    } else {
      body[0].style.overflow = "auto";
    }
  }, [isOpen]);

  useEffect(() => {
    console.log(`data: ${data}`);
  }, [data]);

  useEffect(() => {
    if (isOpenModal) {
      setIsOpen(true);
    }
  }, [isOpenModal]);

  return (
    <>
      {isOpen && (
        <ModalWrapper>
          <PrivacyWrapper>
            <Header>
              <Title>{data?.fields?.title}</Title>
              <IconWrapper>
                <GrClose onClick={handleClose} color="white" />
              </IconWrapper>
            </Header>
            <Content>
              {documentToReactComponents(data?.fields?.content)}
            </Content>
          </PrivacyWrapper>
        </ModalWrapper>
      )}
    </>
  );
};

export default PrivacyPolicy;

PrivacyPolicy.propTypes = {
  data: PropTypes.object,
  isOpenModal: PropTypes.bool,
  setModalOpen: PropTypes.func,
};
