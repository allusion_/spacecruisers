import React, { useRef, useEffect, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { StaticImage } from "gatsby-plugin-image";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { MotionPathPlugin } from "gsap/MotionPathPlugin";
import { Sine } from "gsap/all";
import Lottie from "react-lottie-player";
import Social from "../Social/Social";

import Alien1 from "../../assets/character_1_ship.svg";
import Alien2 from "../../assets/character_2_rock.svg";
import Alien3 from "../../assets/character_3_rock.svg";

import { useMediaQuery } from "react-responsive";
import mq from "../../styling/mediaQueries";

const IntroCard = styled("div")(
  {
    background:
      "linear-gradient(180deg, rgba(55,81,129,0) 0%, rgba(48,101,167,0) 0%, rgba(161,48,217,0.8) 100%)",
    boxShadow: "0px 40px 40px -4px rgba(0,0,0,0.37)",
    padding: "40px",
    margin: "0 auto",
    borderRadius: "12px",
    position: "relative",
    zIndex: "1",
  },
  mq({
    width: ["80%", "80%", "60%"],
    marginTop: ["30%", "30%", "auto"],
    fontSize: ["26px", "25px", "30px"],
    lineHeight: ["32px", "32px", "auto"],
  })
);

const IntroWrapper = styled("div")({
  position: "relative",
});

const AlienImage = styled("div")(
  {
    position: "absolute",
    top: "0",
  },
  mq({
    transform: ["scale(0.6)", "scale(0.6)", "scale(1)"],
    "&.left": {
      left: ["-220%", "-220%", "-50%"],
      top: ["-75%", "-75%", "-60^"],
    },
    "&.top": {
      right: ["140%", "140%", "60%"],
      top: ["-25%", "-25%", "-280px"],
    },
    "&.right": {
      right: ["100%", "100%", "32%"],
      top: ["50%", "50%", "-100px"],
      zIndex: "2",
    },
  })
);

const Intro = ({ text }) => {
  const ref = useRef(null);
  const [isAlien1Finished, setIsAlien1Finished] = useState(false);
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1025px)" });
  gsap.registerPlugin(ScrollTrigger);
  gsap.registerPlugin(MotionPathPlugin);

  const levitateElements = () => {
    const randomX = random(1, 10);
    const randomY = random(1, 10);
    const randomDelay = random(0, 1);
    const randomTime = random(3, 5);
    const randomTime2 = random(5, 10);
    const randomAngle = random(-10, 10);

    const cans = gsap.utils.toArray(".alien");
    cans.forEach((can) => {
      gsap.set(can, {
        x: randomX(-1),
        y: randomX(1),
        rotation: randomAngle(-1),
      });

      moveX(can, 1);
      moveY(can, -1);
      rotate(can, 1);
    });

    function rotate(target, direction) {
      gsap.to(target, randomTime2(), {
        rotation: randomAngle(direction),
        // delay: randomDelay(),
        ease: Sine.easeInOut,
        onComplete: rotate,
        onCompleteParams: [target, direction * -1],
      });
    }

    function moveX(target, direction) {
      gsap.to(target, randomTime(), {
        x: randomX(direction),
        ease: Sine.easeInOut,
        onComplete: moveX,
        onCompleteParams: [target, direction * -1],
      });
    }

    function moveY(target, direction) {
      gsap.to(target, randomTime(), {
        y: randomY(direction),
        ease: Sine.easeInOut,
        onComplete: moveY,
        onCompleteParams: [target, direction * -1],
      });
    }

    function random(min, max) {
      const delta = max - min;
      return (direction = 1) => (min + delta * Math.random()) * direction;
    }
  };

  useEffect(() => {
    const element = ref.current;
    gsap.to(element.querySelector(".left"), {
      // opacity: 1,
      // // x: -100,
      // x: 0,
      motionPath: {
        path: "M 550 600 A 50 50 0 1 1 450 150 C 475 150 800 250 650 400 ",
      },
      scrollTrigger: {
        trigger: element.querySelector(".introcard"),
        start: "top 90%",
        end: "top 40%",
        scrub: true,
        onUpdate: (self) => {
          if (self.progress == 1) {
            setIsAlien1Finished(true);
          }
        },
      },
    });
  }, []);

  useEffect(() => {
    const element = ref.current;
    gsap.to(
      element.querySelector(".right"),

      {
        motionPath: {
          path: "M 750 550 C 475 150 800 250 400 200 ",
        },
        scrollTrigger: {
          trigger: element.querySelector(".introcard"),
          start: "top 90%",
          end: "top 55%",
          scrub: true,
        },
      }
    );
  }, []);

  useEffect(() => {
    const element = ref.current;
    gsap.to(
      element.querySelector(".top"),

      {
        motionPath: {
          path: "M 650 550 C 475 150 800 250 550 100 ",
        },
        scrollTrigger: {
          trigger: element.querySelector(".introcard"),
          start: "top 80%",
          end: "top center",
          scrub: true,
        },
      }
    );
  }, []);

  useEffect(() => {
    if (isAlien1Finished) {
      levitateElements();
    }
  }, [isAlien1Finished]);

  return (
    <IntroWrapper ref={ref}>
      <AlienImage className="left">
        {/* <Lottie
          rendererSettings={{ preserveAspectRatio: "xMidYMid slice" }}
          loop
          animationData={Alien3}
          play
        /> */}
        {/* <StaticImage
          className="alien"
          src="../../assets/character_1_ship.svg"
          alt="Spacecruiser"
          placeholder="blurred"
          layout="fixed"
          height={300}
        /> */}
        <img
          height={300}
          className="alien"
          style={{ transition: "all 1s" }}
          src={Alien1}
          alt="Spacecruisers"
        />
      </AlienImage>
      <IntroCard className="introcard">
        {text}{" "}
        {isTabletOrMobile && (
          <div style={{ marginTop: 55 }}>
            <Social />
          </div>
        )}
      </IntroCard>
      <AlienImage className="top">
        <img
          height={300}
          style={{ transition: "all 1s" }}
          src={Alien2}
          alt="Spacecruisers"
        />
        {/* <StaticImage
          className="alien"
          src="../../assets/character_2_rock.svg"
          alt="Spacecruiser"
          placeholder="blurred"
          layout="fixed"
          height={300}
        /> */}
      </AlienImage>
      <AlienImage className="right">
        <img
          height={300}
          style={{ transition: "all 1s" }}
          src={Alien3}
          alt="Spacecruisers"
        />
        {/* <StaticImage
          className="alien"
          src="../../assets/character_3_rock.svg"
          alt="Spacecruiser"
          placeholder="blurred"
          layout="fixed"
          height={300}
        /> */}
      </AlienImage>
    </IntroWrapper>
  );
};

export default Intro;

Intro.propTypes = {
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
};
