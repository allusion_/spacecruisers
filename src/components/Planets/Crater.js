import React from "react";
import PropTypes from "prop-types";
import styled, { keyframes, css } from "styled-components";
import Lottie from "react-lottie-player";
import mq from "../../styling/mediaQueries";
import CraterImage from "../../assets/crater_empty.svg";
import { StaticImage } from "gatsby-plugin-image";

const Pulse = keyframes`
  0% {
    background-color: #7f0a91;
  }
  50% {
    background-color: #cb2ce3;
  }
  100 {
    background-color: #7f0a91;
  }
`;

const CraterElement = ({ size, rotation }) => {
  const CraterElementWrapper = styled("div")({
    minWidth: size ? 200 * size : 200,
    ".st0": {
      fill: "url(#SVGID_1_)",
    },
    st1: {
      fill: "url(#SVGID_00000093148169166947378850000009367070627723609526_)",
    },
    ".st2": {
      opacity: "0.5",
      fill: "#29338F",
      enableBackground: "new",
    },
    ".st3": {
      fill: "#77858F",
    },
    ".st4": {
      opacity: "0.5",
      fill: "#473873",
      enableBackground: "new",
    },
  });
  return (
    <CraterElementWrapper>
      {/* <StaticImage
        src="../../assets/crater_empty.svg"
        alt="Crater"
        placeholder="blurred"
        layout="fullWidth"
        style={{
          transition: "all 0.8s",
        }}
      /> */}
      <img
        style={{ transition: "all 1s" }}
        src={CraterImage}
        alt="Spacecruisers"
      />
    </CraterElementWrapper>
  );
};

const Crater = ({ size, rotation, position, animationSpeed }) => {
  const CraterWrapper = styled("div")({
    minWidth: size ? 200 * size : 200,
    maxWidth: size ? 200 * size : 200,
    transform: `rotate(${rotation}deg)`,
    position: "absolute",
    top: `${position.top}px`,
    left: `${position.left}px`,
    right: `${position.right}px`,
  });
  const ElementsWrapper = styled("div")(
    {
      position: "relative",
      width: "100%",
      height: "100%",
      display: "flex",
      flexDirection: "column",
      textAlign: "center",
      justifyContent: "center",
      alignItems: "center",
    },
    mq({
      zIndex: ["-1", "-1", "0"],
    })
  );
  // const AnimationWrapper = styled("div")({
  //   minWidth: size ? 200 * size : 200,
  //   bottom: 0,
  //   left: "30%",
  //   transform: "rotate(10deg)",
  //   position: "absolute",
  // });
  const CraterBackground = styled("div")({
    background: "#7f0a91",
    width: "60%",
    height: "25%",
    marginTop: -8,
    position: "absolute",
    transform: "rotate(-15deg)",
    zIndex: -1,
    animation: css`
      ${Pulse} ${animationSpeed}s ease infinite
    `,
  });
  return (
    <CraterWrapper>
      <ElementsWrapper>
        {/* <AnimationWrapper>
          <Lottie loop animationData={anim} />
        </AnimationWrapper> */}
        <CraterElement rotation={rotation} size={size} />
        <CraterBackground />
      </ElementsWrapper>
    </CraterWrapper>
  );
};

export default Crater;

Crater.propTypes = {
  size: PropTypes.number,
  rotation: PropTypes.number,
  position: PropTypes.objectOf(PropTypes.number),
  animationSpeed: PropTypes.number,
};

Crater.defaultProps = {
  size: 1,
  rotation: -9,
  animationSpeed: 2,
  position: {
    top: 0,
    left: 0,
  },
};
