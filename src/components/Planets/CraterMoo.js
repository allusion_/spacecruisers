import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Lottie from "react-lottie-player";

import Tehen1 from "../../assets/animarions/Tehen.json";
import Tehen2 from "../../assets/animarions/Tehen3.json";

const CraterElement = ({ size, rotation }) => {
  const CraterElementWrapper = styled("div")({
    minWidth: size ? 200 * size : 200,
    transform: "rotate(180deg)",
    ".st0": {
      fill: "url(#SVGID_1_)",
    },
    st1: {
      fill: "url(#SVGID_00000110448721319879415070000000823779348675935124_)",
    },
    ".st2": {
      opacity: "0.5",
      fill: "#29338F",
      enableBackground: "new",
    },
    ".st3": {
      fill: "#77858F",
    },
    ".st4": {
      opacity: "0.5",
      fill: "#473873",
      enableBackground: "new",
    },
  });
  return (
    <CraterElementWrapper>
      <svg
        version="1.1"
        id="Layer_1"
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        x="0px"
        y="0px"
        viewBox="0 0 1000 327.4"
        style={{ enableBackground: "new 0 0 1000 327.4" }}
        xmlSpace="preserve"
      >
        <g>
          <linearGradient
            id="SVGID_1_"
            gradientUnits="userSpaceOnUse"
            x1="564.0727"
            y1="-51.4931"
            x2="433.6469"
            y2="298.2859"
            gradientTransform="matrix(1 0 0 -1 0 330)"
          >
            <stop offset="0" style={{ stopColor: "#0E818C" }} />
            <stop offset="0.1747" style={{ stopColor: "#166E89" }} />
            <stop offset="0.3882" style={{ stopColor: "#1D5F86" }} />
            <stop offset="0.6366" style={{ stopColor: "#215684" }} />
            <stop offset="1" style={{ stopColor: "#225384" }} />
          </linearGradient>
          <path
            className="st0"
            d="M889.8,184.7c-52.5-30.2-116.7-61.5-192.8-87.6c-219-75-420.2-68.8-537.7-56.3C113.6,63.4,67.9,86,22.5,108.3
  c82.1,45.4,210.2,103.9,379.1,137.1c240.4,47.3,442,19,550.2-3.3C931.1,223.1,910.5,203.7,889.8,184.7z M373,157.5
  c-24.5-8.4-47.3-17.7-68.5-27.2c32.6-7.3,72.1-12.8,116.9-12.2c135.4,1.4,236.1,56.3,285.6,89.2C612.7,210,495.7,200.2,373,157.5z"
          />

          <linearGradient
            id="SVGID_00000157997480635667549210000006082512434660367760_"
            gradientUnits="userSpaceOnUse"
            x1="516.6799"
            y1="130.8474"
            x2="538.9418"
            y2="338.6219"
            gradientTransform="matrix(1 0 0 -1 0 330)"
          >
            <stop offset="0" style={{ stopColor: "#1C4476" }} />
            <stop offset="6.793034e-02" style={{ stopColor: "#1D3C6C" }} />
            <stop offset="0.2449" style={{ stopColor: "#1F2C56" }} />
            <stop offset="0.4407" style={{ stopColor: "#202047" }} />
            <stop offset="0.6678" style={{ stopColor: "#21193E" }} />
            <stop offset="1" style={{ stopColor: "#21173B" }} />
          </linearGradient>
          <path
            style={{
              fill: "url(#SVGID_00000157997480635667549210000006082512434660367760_)",
            }}
            d="M421.5,118.1
  c135.1,1.4,235.5,56,285.2,88.9h0.1c69.9-1.9,127.3-10.6,165.4-17.7C817,158,746,124.6,659.2,98.2C459.3,37.6,280,46.8,177.2,59.3
  c34,23.1,76.4,48.1,127,70.7c0.2,0.1,0.3,0.1,0.5,0.2C337.3,122.9,376.7,117.5,421.5,118.1z"
          />
          <path
            className="st2"
            d="M304.5,130.3c21.2,9.5,44.1,18.8,68.5,27.2c122.7,42.7,239.6,52.5,334,49.8c-49.5-32.9-150.1-87.8-285.6-89.2
  C376.6,117.5,337.1,123,304.5,130.3z"
          />
          <path
            className="st3"
            d="M648.8,80.8c-19.3-6.3-38.6-12-58.5-16.9c-19.6-5.2-39.7-9.5-59.8-13.6c-20.1-4.1-40.3-7.3-60.7-10.3
  c-10.1-1.6-20.4-2.7-30.5-4.1c-10.3-1.1-20.7-2.4-30.7-3.3c-10.3-1.1-20.7-1.6-31-2.4c-10.3-0.5-20.7-1.4-31-1.6
  c-10.3-0.5-20.7-0.8-31-0.8s-20.7-0.3-31,0c-20.7,0.3-41.3,0.8-62,2.2c-10.3,0.5-20.7,1.4-31,2.2c-10.3,0.8-20.7,1.9-30.7,3h-0.3
  c-0.8,0-1.4,0.3-1.9,0.5l-34.5,18l-34.5,18L55.4,89.8C44,96,32.5,102,21.4,108.6c12-5.2,24.2-10.3,36.2-15.5l35.9-16l35.6-16.3
  l35.6-16.3l-2.2,0.5c10.1-0.8,20.4-1.4,30.5-1.9s20.4-1.1,30.7-1.6c10.3-0.3,20.4-0.5,30.7-0.8s20.4-0.3,30.7-0.3s20.4,0,30.7,0.3
  c10.3,0.3,20.4,0.5,30.7,0.8c20.4,0.8,40.8,1.9,61.2,3.5c10.3,0.5,20.4,1.6,30.5,2.4c10.3,1.1,20.4,2.2,30.5,3.3s20.1,2.4,30.5,3.8
  c10.1,1.4,20.1,3,30.2,4.4s20.1,3.3,30.2,4.9c10.1,1.9,20.1,3.5,30.2,5.4C608.9,72.4,629,76.5,648.8,80.8z"
          />
          <path
            className="st4"
            d="M22.2,106.9C86.7,152.6,177,205.4,295,244.3c302.7,99.8,579.6,40,684.6,12c-17.1-12.5-34.3-24.7-51.4-37.3
  c-27.2,6.3-55.2,12-84,16.6C516.9,287.8,221.3,196.1,22.2,106.9z"
          />
        </g>
      </svg>
    </CraterElementWrapper>
  );
};

const CraterMoo = ({ size, rotation, position, type }) => {
  // const [playAnimation, setPlayAnimation] = useState(false);
  // const lottiePlayer = useRef();

  // const interval = setInterval(function () {
  //   setPlayAnimation(true);
  // }, 10000);

  // useEffect(() => {
  //   setTimeout(() => {
  //     setPlayAnimation(true);
  //   }, 10000);
  // }, [playAnimation]);

  // useEffect(() => {
  //   console.log(lottiePlayer.current);
  // }, [playAnimation]);

  const CraterWrapper = styled("div")({
    minWidth: size ? 200 * size : 200,
    maxWidth: size ? 200 * size : 200,
    transform: `rotate(${rotation}deg)`,
    position: "absolute",
    top: `${position.top}px`,
    left: `${position.left}px`,
    right: `${position.right}px`,
  });
  const ElementsWrapper = styled("div")({
    position: "relative",
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
  });
  const AnimationWrapper = styled("div")({
    minWidth: size ? 600 * size : 600,
    top: -40,
    left: "-120%",
    transform: "rotate(10deg)",
    position: "absolute",
    zIndex: 1,
  });
  return (
    <CraterWrapper>
      <ElementsWrapper>
        <AnimationWrapper>
          <Lottie
            rendererSettings={{ preserveAspectRatio: "xMidYMid slice" }}
            loop
            animationData={type === 2 ? Tehen2 : Tehen1}
            play
          />
        </AnimationWrapper>

        <CraterElement rotation={rotation} size={size} />
      </ElementsWrapper>
    </CraterWrapper>
  );
};

export default CraterMoo;

CraterMoo.propTypes = {
  size: PropTypes.number,
  rotation: PropTypes.number,
  position: PropTypes.objectOf(PropTypes.number),
  type: PropTypes.number,
};

CraterMoo.defaultProps = {
  size: 1,
  rotation: -9,
  position: {
    top: 0,
    left: 0,
  },
};
