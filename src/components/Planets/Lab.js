import React from "react";
import styled from "styled-components";
import { StaticImage } from "gatsby-plugin-image";
import mq from "../../styling/mediaQueries";

const LabWrapper = styled("div")(
  {
    position: "relative",
  },
  mq({
    marginLeft: ["-200%", "-200%", 0],
    marginTop: [-100, -100, 0],
    width: [100, 100, 200],
    height: [100, 100, 200],
  })
);

const Lab = () => {
  return (
    <LabWrapper>
      <StaticImage
        src="../../assets/lab.svg"
        alt="Spacecruisers"
        placeholder="blurred"
        layout="fullWidth"
        style={{
          transition: "all 0.8s",
        }}
      />
    </LabWrapper>
  );
};

export default Lab;
