import React from "react";
import styled from "styled-components";

const MeteorWrapper = styled.div`
  @include keyframes(meteor-drop) {
    0% {
      opacity: 0;
      top: 50px;
      left: 1250px;
    }
    50% {
      opacity: 1;
    }
    100% {
      left: 800px;
      top: 550px;
      opacity: 0;
    }
  }

  @include keyframes(meteor-drop2) {
    0% {
      opacity: 0;
      top: 150px;
      left: 1350px;
    }
    50% {
      opacity: 1;
    }
    100% {
      left: 900px;
      top: 650px;
      opacity: 0;
    }
  }

  @include keyframes(planet-moving) {
    0% {
      top: 30%;
    }
    50% {
      top: 31%;
    }
    100% {
      top: 30%;
    }
  }

  @include keyframes(planet-moving2) {
    0% {
      top: 30%;
    }
    50% {
      top: 31%;
    }
    100% {
      top: 30%;
    }
  }
  @include keyframes(twinkle-height) {
    0% {
      height: 20px;
      top: -7px;
    }
    50% {
      height: 10px;
      top: -3px;
    }
    100% {
      height: 20px;
      top: -7px;
    }
  }

  @include keyframes(twinkle-width) {
    0% {
      width: 20px;
      left: 0px;
    }
    50% {
      width: 10px;
      left: 5px;
    }
    100% {
      width: 20px;
      left: 0px;
    }
  }

  @include keyframes(twinkle) {
    0% {
      opacity: 0;
    }
    50% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }
  position: absolute;
  width: auto;
  height: auto;
  top: 800px;
  left: 550px;
  @include animation(meteor-drop 2s ease infinite);
  @include animation-delay(2s);
  @include transform(rotate(40deg));
  opacity: 0;

  div {
    @include transition(all 0.3s ease);
  }
  div:nth-child(1) {
    @include background(linear-gradient(to top, $purple2, transparent));
    height: 150px;
    width: 20px;
    @include border-radius(100px);
    opacity: 0.8;
    position: absolute;
    left: -5.5px;
    top: -96px;
    z-index: -2;
  }

  div:nth-child(2) {
    //meteor second tail
    width: 12px;
    height: 100px;
    @include background(linear-gradient(to top, $fuscia, transparent));
    @include border-radius(500px);
    position: absolute;
    opacity: 0.8;
    left: -1.5px;
    top: -53px;
  }

  div:nth-child(3) {
    //meteor circle
    width: 8px;
    height: 8px;
    background: $meteor-yellow;
    @include border-radius(100px);
    top: 35px;
    position: absolute;
    z-index: 2;
  }
  div:nth-child(4) {
    //meteor tail
    width: 0;
    height: 0;
    border-left: 4px solid transparent;
    border-right: 5px solid transparent;
    border-bottom: 30px solid $meteor-yellow;
    z-index: 1;
    top: 8px;
    position: absolute;
  }
`;

const Meteor = () => {
  return (
    <MeteorWrapper>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </MeteorWrapper>
  );
};

export default Meteor;
