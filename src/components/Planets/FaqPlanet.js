import React from "react";
import styled, { keyframes, css } from "styled-components";
import FaqPlanetTextureImage from "../../assets/FAQ_texture.svg";

const Pulsing = keyframes`
  0% { box-shadow: 0 0 45px 50px #3781d133; }
	50% { box-shadow: 0 0 45px 50px rgba(63, 134, 200, 0.6); }
	100% { box-shadow: 0 0 45px 50px #218bd133; }
`;

const Rotate = keyframes`
  100% { transform:rotate(-360deg); transition: transform 20s; }
`;

// 0% { box-shadow: 0 0 15px 50px #5399bc33; }

const FaqPlanetTexture = styled("div")({
  width: "3700px",
  height: "3700px",
  position: "absolute",
  backgroundImage: `url(${FaqPlanetTextureImage}) `,
  backgroundRepeat: "no-repeat",
  backgroundSize: "3700px 3000px",
  top: "-20%",
  left: "-10%",
  bottom: "-40%",
  mixBlendMode: "multiply",
  zIndex: -1,
  animation: css`
    ${Rotate} 400s infinite linear both
  `,
});

const FaqPlanetElement = styled("div")({
  width: 2700,
  height: 2700,
  background:
    "radial-gradient(circle, rgba(72,70,113,1) 0%, rgba(43,23,87,1) 100%);",
  borderRadius: "100%",
  position: "absolute",
  top: "10%",
  right: "-20%",
  bottom: "-40%",
  zIndex: -2,
  // animation: css`
  //   ${Rotate} 400s infinite linear both
  // `,
});

const FaqPlanetPulsating = styled("div")({
  width: 2700,
  height: 2700,
  position: "absolute",
  borderRadius: "100%",
  zIndex: -3,
  top: "10%",
  right: "-20%",
  bottom: "-40%",
  animation: css`
    ${Pulsing} 10s ease infinite
  `,
});

const FaqPlanet = () => {
  return (
    <div style={{ paddingTop: 150 }}>
      <FaqPlanetElement>
        <FaqPlanetTexture />
      </FaqPlanetElement>
      <FaqPlanetPulsating />
    </div>
  );
};

export default FaqPlanet;
