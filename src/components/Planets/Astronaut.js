import React from "react";
import PropTypes from "prop-types";
import styled, { keyframes, css } from "styled-components";
import Lottie from "react-lottie-player";

import AstronautAlien from "../../assets/animarions/Spaceguy.json";

const RotateAstronaut = keyframes`
  100% { transform:rotate(-720deg) }
`;

const MoveAstronaut = keyframes`
   100% {transform:translate(-160px, -160px)}
`;

const Astronaut = ({ size, rotateSpeed }) => {
  const AstronautWrapper = styled("div")({
    display: "block",
    width: size ? size * 300 : 300,
    animation: css`
      ${MoveAstronaut} 50s infinite linear both alternate
    `,
    ".astronaut": {
      animation: css`
        ${RotateAstronaut} ${rotateSpeed}s infinite linear both alternate
      `,
    },
  });

  return (
    <AstronautWrapper>
      <Lottie
        className="astronaut"
        rendererSettings={{ preserveAspectRatio: "xMidYMid slice" }}
        loop
        animationData={AstronautAlien}
        play
      />
    </AstronautWrapper>
  );
};

export default Astronaut;

Astronaut.propTypes = {
  size: PropTypes.number,
  rotateSpeed: PropTypes.number,
};

Astronaut.defaultProps = {
  size: 1,
  rotateSpeed: 200,
};
