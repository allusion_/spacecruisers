import React from "react";
import styled, { keyframes } from "styled-components";

const MoveClouds = keyframes`
  from {
    background-position: 0 0;
  }
  to {
    background-position: 10000px 0;
  }
`;

const CloudsWrapper = styled("div")({});

const CloudsElements = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  display: block;
  overflow: hidden;
  background: transparent url(../../assets/clouds3.png) repeat top center;
  z-index: -4;
  mix-blend-mode: overlay;
  opacity: 0.3;
  animation: ${MoveClouds} 200s linear infinite;
`;

const Clouds = () => {
  return (
    <CloudsWrapper>
      <CloudsElements />
    </CloudsWrapper>
  );
};

export default Clouds;
