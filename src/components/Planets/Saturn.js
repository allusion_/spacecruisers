import React from "react";
import styled from "styled-components";
import { StaticImage } from "gatsby-plugin-image";

const PlanetWrapper = styled("div")({
  position: "relative",
  width: 200,
  height: 200,
  opacity: 0.8,
});

const Saturn = () => {
  return (
    <PlanetWrapper>
      <StaticImage
        src="../../assets/saturn.svg"
        alt="Spacecruisers"
        placeholder="blurred"
        layout="fullWidth"
        style={{
          transition: "all 0.8s",
        }}
      />
    </PlanetWrapper>
  );
};

export default Saturn;
