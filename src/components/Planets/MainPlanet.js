import React from "react";
import styled, { keyframes, css } from "styled-components";
import { StaticImage } from "gatsby-plugin-image";

import { constrains } from "../../styling/variables";

const MoveUpDownSmall = keyframes`
  0% {
    transform: translateY(-10px);
  }

  50% {
    transform: translateY(10px);
  }

  100% {
    transform: translateY(-10px);
  }
`;

const MainPlanetWrapper = styled("div")({
  top: constrains.mainPlanetTop,
  width: "100%",
  position: "absolute",
  zIndex: "-1",
  animation: css`
    ${MoveUpDownSmall} 7s ease infinite;
  `,
});

const MainPlanet = () => {
  return (
    <MainPlanetWrapper>
      <StaticImage
        src="../../assets/Moon-tinified.png"
        alt="Spacecruisers"
        placeholder="blurred"
        layout="fullWidth"
      />
    </MainPlanetWrapper>
  );
};

export default MainPlanet;
