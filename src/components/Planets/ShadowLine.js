import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { StaticImage } from "gatsby-plugin-image";

const ShadowLineElementWrapper = styled("div")({
  ".st0": {
    opacity: "0.85",
    fill: "#095772",
    enableBackground: "new",
  },
});

const ShadowLineElement = () => {
  return (
    <ShadowLineElementWrapper>
      <StaticImage
        src="../../assets/shadowline.svg"
        alt="Crater"
        placeholder="blurred"
        layout="fullWidth"
        style={{
          transition: "all 0.8s",
        }}
      />
    </ShadowLineElementWrapper>
  );
};

const ShadowLine = ({ rotation, size, position }) => {
  const ShadowWrapper = styled("div")({
    position: "absolute",
    width: `calc(100% * ${size})`,
    transform: `rotate(${rotation}deg)`,
    top: `${position.top}px`,
    left: `${position.left}px`,
    right: `${position.right}px`,
  });
  return (
    <ShadowWrapper>
      <ShadowLineElement />
    </ShadowWrapper>
  );
};

export default ShadowLine;

ShadowLine.propTypes = {
  rotation: PropTypes.number,
  size: PropTypes.number,
  position: PropTypes.objectOf(PropTypes.number),
};

ShadowLine.defaultProps = {
  rotation: 0,
  size: 1,
  position: {
    top: 0,
    left: 0,
  },
};
