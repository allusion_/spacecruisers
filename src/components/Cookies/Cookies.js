import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { CookieNotice } from "gatsby-cookie-notice";
import { MaxWidthContainer } from "../../styling/layout";
import { colors } from "../../styling/variables";

const CookiesContainer = styled("div")({
  position: "fixed",
  zIndex: 97,
  bottom: 0,
  width: "100%",
  ".container": {
    background:
      "linear-gradient(180deg, rgba(63,119,132,1) 0%, rgba(55,81,129,1) 100%)",
    padding: 15,
  },
  ".personalise": {
    display: "none",
  },
  ".button": {
    display: "flex",
    justifyContent: "flex-end",
    width: "100%",
    button: {
      padding: "15px 100px",
      background: "none",
      color: "white",
      border: `1px solid ${colors.purple}`,
      fontSize: 22,
      cursor: "pointer",
      borderRadius: 40,
      transition: "all 0.5s",
      "&:hover": {
        background: colors.purple,
      },
    },
  },
  p: {
    fontSize: 22,
    lineHeight: "26px",
  },
  h4: {
    fontSize: 26,
    margin: 0,
  },
});

const Cookies = ({ setPolicy }) => {
  const handleShowPolicy = () => {
    setPolicy(true);
  };

  return (
    <CookiesContainer>
      <CookieNotice
        buttonWrapperClasses="button"
        personalizeButtonClasses="personalise"
        declineButton={false}
        cookies={[
          {
            name: "gatsby-gdpr-google-analytics",
            editable: true,
            default: true,
            title: "Google Analytics",
            text: "Google Analytics is a statistical tool of Google allowing to measure the audience of the website.",
          },
        ]}
      >
        <MaxWidthContainer>
          <h4>This websites uses cookies.</h4>
          <p>
            We use cookies to make the site work better, but also to see how you
            interact with it. We will only use cookies if you allow us to do so
            by clicking by clicking on "Accept".
            <br />
            Read our{" "}
            <span
              onClick={handleShowPolicy}
              style={{ fontWeight: "bold", cursor: "pointer" }}
            >
              Our cookie policy
            </span>
          </p>
        </MaxWidthContainer>
      </CookieNotice>
    </CookiesContainer>
  );
};

export default Cookies;

Cookies.propTypes = {
  setPolicy: PropTypes.func,
};
