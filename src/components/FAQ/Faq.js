import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Gradient from "rgt";
import { colors } from "../../styling/variables";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { MaxWidthContainer } from "../../styling/layout";
import { HiOutlineChevronDown } from "react-icons/hi";
import mq from "../../styling/mediaQueries";
import { useMediaQuery } from "react-responsive";
import FaqPlanet from "../Planets/FaqPlanet";

import { useSpring, animated } from "react-spring";

const FaqWrapper = styled("div")(
  {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    position: "relative",
    overflow: "hidden",
  },
  mq({
    marginTop: [0, 0, 0],
    padding: ["50px 0", "50px 0", "300px 0"],
    margin: ["20px 0", "20px 0", "100px 0 0 0"],
  })
);

const FaqContent = styled("div")({
  width: "100%",
});

const FaqElementWrapper = styled("div")({
  margin: "15px 0",
  width: "100%",
  overflow: "hidden",
  position: "relative",
  zIndex: 1,
});

const FaqElement = ({ question, answer }) => {
  const [open, setOpen] = useState(false);
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1025px)" });

  const FaqQuestion = styled("div")(
    {
      background:
        "linear-gradient(90deg, rgba(27,13,52,1) 0%, rgba(56,72,119,1) 100%)",
      padding: 30,
      borderRadius: !open ? "8px" : "8px 8px 0 0",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      fontWeight: 500,
      cursor: "pointer",
    },
    mq({
      fontSize: [15, 15, 26],
    })
  );

  const FaqAnswer = styled("div")({
    background:
      "linear-gradient(90deg, rgba(27,13,52,1) 0%, rgba(56,72,119,1) 100%)",
    padding: "0 30px 30px 30px",
    fontSize: 20,
    borderRadius: "0 0 8px 8px",
    lineHeight: "28px",
    transition: "all 0.3s ease-in-out",
    p: {
      margin: 0,
    },
  });

  let toggleHandler = (e) => {
    //switch state
    setOpen(!open);
  };

  const openAnimation = useSpring({
    from: { opacity: "0", maxHeight: !isTabletOrMobile ? "100px" : "800px" },
    to: {
      opacity: "1",
      maxHeight: open
        ? `${!isTabletOrMobile ? "400px" : "800px"}`
        : `${!isTabletOrMobile ? "100px" : "80"}`,
    },
    config: { duration: "300" },
  });

  const iconAnimation = useSpring({
    from: {
      transform: "rotate(0deg)",
    },
    to: {
      transform: open ? "rotate(180deg)" : "rotate(0deg)",
    },
    config: { duration: "200" },
  });

  return (
    <FaqElementWrapper>
      <animated.div style={openAnimation}>
        <FaqQuestion onClick={toggleHandler}>
          {question}
          <animated.i style={iconAnimation}>
            <HiOutlineChevronDown />
          </animated.i>
        </FaqQuestion>
        <FaqAnswer>{answer}</FaqAnswer>
      </animated.div>
    </FaqElementWrapper>
  );
};

const Faq = ({ faqElements }) => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1025px)" });
  return (
    <FaqWrapper>
      <MaxWidthContainer>
        <h2 style={{ textAlign: "center" }}>
          <Gradient dir="left-to-right" from={colors.blue} to={colors.purple}>
            FAQ
          </Gradient>
        </h2>

        <FaqContent>
          {faqElements?.map((element) => (
            <FaqElement
              key={element.fields.id}
              question={element.fields.question}
              answer={documentToReactComponents(element?.fields?.answer)}
            />
          ))}
        </FaqContent>
      </MaxWidthContainer>
      <FaqPlanet />
    </FaqWrapper>
  );
};

export default Faq;

Faq.propTypes = {
  faqElements: PropTypes.array,
};

FaqElement.propTypes = {
  question: PropTypes.string,
  answer: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
