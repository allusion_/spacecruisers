import React, { useState } from "react";
import { MaxWidthContainer } from "../../styling/layout";
import styled from "styled-components";
import { sizes } from "../../styling/variables";
import Logo from "../Logo/Logo";
import Menu from "../Menu/Menu";
import MenuMobile from "../Menu/MenuMobile";
import { useScrollPosition } from "../../hooks/useScrollPosition";
import { AnchorLink } from "gatsby-plugin-anchor-links";
import { useMediaQuery } from "react-responsive";

const HeaderWrapper = styled("header")({
  // display: "flex",
  // justifyContent: "space-between",
  // alignItems: "center",
  height: sizes.headerHeight,
  position: "fixed",
  top: 0,
  left: 0,
  width: "100%",
  maxWidth: "100vw",
  zIndex: 3,
  // background: "transparent",

  background:
    "linear-gradient(0deg, rgba(0,0,0,0) 0%, rgba(0,0,0,0.7231267507002801) 100%)",
  "&.scroll": {
    height: sizes.headerHeightScroll,
    background:
      "linear-gradient(0deg, rgba(0,116,119,0) 0%, rgba(0,0,0,1) 100%)",
  },
});

const Inline = styled("div")({
  display: "inline-block",
  "&.left": {
    float: "left",
  },
  "&.right": {
    float: "right",
  },
});

const MenuItems = [
  {
    title: "introduction",
    link: "introduction",
  },
  {
    title: "about us",
    link: "about-us",
  },
  {
    title: "cruiseville",
    link: "cruiseville",
  },
  {
    title: "roadmap",
    link: "roadmap",
  },
  {
    title: "faq",
    link: "faq",
  },
];

const Header = () => {
  const [scroll, setScroll] = useState(0);
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1025px)" });
  useScrollPosition(function setScrollPosition({ currentPosition }) {
    setScroll(currentPosition.y);
  });

  return (
    <div id="header">
      <HeaderWrapper className={scroll <= -300 ? "scroll" : ""}>
        <MaxWidthContainer>
          <Inline className="left">
            <AnchorLink to={`/#header`} title="Back to space!">
              <Logo isSmaller={scroll <= -300} />
            </AnchorLink>
          </Inline>
          <Inline className="right">
            <Menu
              menuItems={MenuItems}
              isSmaller={scroll <= -300 ? "scroll" : ""}
            />
            {isTabletOrMobile && (
              <MenuMobile menuItems={MenuItems} isSmaller={scroll <= -300} />
            )}
          </Inline>
        </MaxWidthContainer>
      </HeaderWrapper>
    </div>
  );
};

export default Header;
