import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { MaxWidthContainer } from "../../styling/layout";
import Intro from "../Intro/Intro";
import mq from "../../styling/mediaQueries";

const SectionWrapper = styled("section")(
  {
    width: "100%",
    textAlign: "center",
    position: "relative",
    zIndex: 2,
  },
  mq({
    fontSize: ["26px", "26px", "30px"],
    lineHeight: ["32px", "32px", "auto"],
  })
);

const SectionContent = styled("div")({
  fontSize: "30px",
});

const NarrowSection = styled("div")(
  {
    margin: "0 auto",
    textAlign: "center",
    position: "relative",
  },
  mq({
    width: ["90%", "90%", "70%"],
  })
);

const Section = ({ title, text, sectionName }) => {
  return (
    <SectionWrapper>
      <h2>{title}</h2>
      {sectionName === "intro" && (
        <>
          <MaxWidthContainer>
            <Intro text={text} />
          </MaxWidthContainer>
        </>
      )}
      {sectionName !== "intro" && (
        <>
          <MaxWidthContainer>
            <NarrowSection>{text}</NarrowSection>
          </MaxWidthContainer>
        </>
      )}
    </SectionWrapper>
  );
};

Section.propTypes = {
  title: PropTypes.string,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  isIntro: PropTypes.bool,
  sectionName: PropTypes.string,
};

Section.defaultProps = {
  isIntro: false,
};

export default Section;
