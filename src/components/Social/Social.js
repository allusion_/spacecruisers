import React from "react";
import styled from "styled-components";
import { FaTwitter, FaDiscord, FaInstagram } from "react-icons/fa";
import { colors } from "../../styling/variables";
import mq from "../../styling/mediaQueries";

const SocialWrapper = styled("div")({
  display: "flex",
  height: "100%",
  alignItems: "center",
  marginBottom: -15,
});

const SocialElement = styled("div")(
  {
    fontSize: "24px",
    a: {
      color: colors.light,
      transition: "all 0.5s",
      "&:hover": {
        color: colors.purple,
      },
    },
  },
  mq({
    margin: ["0 10px", "0 10px", "0 10px"],
  })
);

const SocialLinks = [
  {
    icon: <FaTwitter />,
    name: "Twitter",
    link: "https://twitter.com/tspacecruisers/",
  },
  {
    icon: <FaDiscord />,
    name: "Discord",
    link: "https://discord.gg/qDRCvST5",
  },
  {
    icon: <FaInstagram />,
    name: "Instagram",
    link: "https://www.instagram.com/thespacecruisers/?igshid=YmMyMTA2M2Y=",
  },
];

const Social = () => {
  return (
    <SocialWrapper>
      {SocialLinks.map((element) => (
        <SocialElement key={element.name}>
          <a
            href={element.link}
            alt={element.name}
            target="_blank"
            rel="noreferrer"
          >
            {element.icon}
          </a>
        </SocialElement>
      ))}
    </SocialWrapper>
  );
};

export default Social;
