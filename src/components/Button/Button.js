import React from "react";
import PropTypes from "prop-types";
import styled, { keyframes, css } from "styled-components";
import { colors } from "../../styling/variables";

// const ButtonWrapper = styled("div")({
//   position: "relative",
//   background: "linear-gradient(to right, red, purple)",
//   padding: 3,
// });

const Ainmate = keyframes`
  0% {background-position 0% 50%}
  50% {background-position 100% 50%}
  100% {background-position 0% 50%}
`;

const Button = ({ text, link, size, blank }) => {
  const ButtonElement = styled("button")({
    border: "none",
    outline: "0",
    boxShadow: "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
    padding: "15px 50px",
    borderRadius: 50,
    color: "white",
    background: `linear-gradient(-45deg, ${colors.blue}, ${colors.purple})`,
    backgroundSize: "600%",
    animation: css`
      ${Ainmate} 10s infinite linear
    `,
    fontSize: size,
    fontWeight: 500,
    transition: "all 0.5s",
    "&:hover": {
      background: "transparent",
      outline: `3px solid ${colors.purple}`,
      cursor: "pointer",
    },
  });

  return (
    <a href={link} target={blank ? "_blank" : "self"} rel="noreferrer">
      <ButtonElement type="button">{text}</ButtonElement>
    </a>
  );
};

export default Button;

Button.propTypes = {
  text: PropTypes.string,
  link: PropTypes.string,
  size: PropTypes.number,
  blank: PropTypes.bool,
};

Button.defaultProps = {
  size: 30,
  blank: true,
};
