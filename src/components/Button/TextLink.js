import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const TextLinKWrapper = styled("div")({
  display: "block",
});

const Link = styled("a")({
  color: "white",
  textDecoration: "none",
});

const TextLink = ({ text, href, alt, blank }) => {
  return (
    <TextLinKWrapper>
      <Link href={href} alt={alt} target={blank ? "_blank" : "self"}>
        {text}
      </Link>
    </TextLinKWrapper>
  );
};

export default TextLink;

TextLink.propTypes = {
  text: PropTypes.string,
  href: PropTypes.string,
  alt: PropTypes.string,
  blank: PropTypes.bool,
};
