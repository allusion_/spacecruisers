import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import Social from "../Social/Social";
import { AnchorLink } from "gatsby-plugin-anchor-links";
import { colors } from "../../styling/variables";
import mq from "../../styling/mediaQueries";
import { useMediaQuery } from "react-responsive";

const MenuWrapper = styled("nav")(
  {
    height: "100%",
    alignItems: "center",
    marginTop: "30px",
  },
  mq({
    display: ["none", "none", "flex"],
  })
);

const MainMenu = styled("ul")({
  listStyle: "none",
  display: "flex",
  margin: "0",
});

const MenuElement = styled("li")({
  margin: "0 10px",
  padding: "0 10px",
  fontSize: "30px",
  transition: "all 0.5s",
  "&.smaller": {
    fontSize: "24px",
  },
  a: {
    color: "white",
    transition: "all 0.5s",
    "&:hover": {
      color: colors.purple,
    },
  },
});

const Menu = ({ isSmaller, menuItems }) => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1025px)" });
  return (
    <MenuWrapper>
      <MainMenu>
        {menuItems.map((item) => (
          <MenuElement className={isSmaller ? "smaller" : ""} key={item.title}>
            <AnchorLink to={`/#${item.link}`} title={item.title}>
              {item.title}
            </AnchorLink>
          </MenuElement>
        ))}
      </MainMenu>
      <Social />
    </MenuWrapper>
  );
};

export default Menu;

Menu.propTypes = {
  isSmaller: PropTypes.string,
  menuItems: PropTypes.array,
};
