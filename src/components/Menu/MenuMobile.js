import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import Social from "../Social/Social";
import PropTypes from "prop-types";
import { AnchorLink } from "gatsby-plugin-anchor-links";
import { Divide as Hamburger } from "hamburger-react";

const MenuMobileWrapper = styled("div")({
  display: "flex",
  alignItems: "center",
  marginTop: 20,
  opacity: 1,
  transition: "all 1s",
  ".smaller": {
    opacity: 0,
    marginTop: -100,
  },
});

const MenuMobile = ({ isSmaller, menuItems }) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const overlay = useRef(null);

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
    const body = document.getElementsByTagName("body");

    if (!menuOpen) {
      body[0].style.overflow = "hidden";
    } else {
      body[0].style.overflow = "auto";
    }
  };

  const MenuToggle = styled("div")({
    marginLeft: 15,
    fontSize: 32,
    zIndex: 8,
    color: "white",
    "&.bigger": {
      fontSize: 44,
    },
  });

  const MobileOverlay = styled("div")({
    color: "black",
    position: "absolute",
    top: 0,
    right: "-100%",
    width: "100%",
    height: "100vh",
    zIndex: 7,
    background:
      "linear-gradient(180deg, rgba(63,119,132,1) 0%, rgba(55,81,129,1) 100%)",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    transition: "all 1s",
    "&.open": {
      right: 0,
    },
  });

  const MenuElement = styled("div")({
    fontSize: 28,
    margin: "15px 0",
    a: {
      color: "white",
      fontWeight: "bold",
    },
  });

  // useEffect(() => {
  //   const element = overlay.current;

  //   gsap.to(element, {
  //     x: "-300px",
  //   });
  // }, [menuOpen]);

  // useEffect(() => {
  //   setMenuOpen(!menuOpen);
  // }, [menuOpen]);

  return (
    <MenuMobileWrapper>
      <div className={isSmaller ? "smaller" : ""}>
        <Social />
      </div>
      <MenuToggle
        onClick={() => toggleMenu()}
        className={isSmaller ? "bigger" : ""}
      >
        <Hamburger rounded size={24} toggled={menuOpen} />
      </MenuToggle>
      <MobileOverlay ref={overlay} className={menuOpen ? "open" : ""}>
        {menuItems.map((item) => (
          <MenuElement key={item.title} onClick={toggleMenu}>
            <AnchorLink to={`/#${item.link}`} title={item.title}>
              {item.title}
            </AnchorLink>
          </MenuElement>
        ))}
      </MobileOverlay>
    </MenuMobileWrapper>
  );
};

export default MenuMobile;

MenuMobile.propTypes = {
  isSmaller: PropTypes.bool,
  menuItems: PropTypes.array,
};
