import React from "react";
import { useEffect } from "react";
import styled, { keyframes, css } from "styled-components";
import { StaticImage } from "gatsby-plugin-image";
import FogImage from "../../assets/fog.png";
import BgImage from "../../assets/fog1.png";

const Spin = keyframes`
  from {
    transform: rotate( 0deg );
  }
   
  to {
    transform: rotate( 360deg );
  }
`;

const MovingStarsWrapper = styled("div")({
  position: "fixed",
  zIndex: "-3",
  width: "400vw",
  height: "400vh",
  top: "50%",
  left: "50%",
  marginTop: "-200vh",
  marginLeft: "-200vw",
  animation: css`
    ${Spin} 500s linear infinite
  `,
  backgroundSize: "240px",
  backfaceVisibility: "visible",

  /* Had to base64 SVG background for FF compatibility */
  backgroundImage:
    "url(data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8yIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0MCAyNDAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDI0MCAyNDAiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxyZWN0IHg9IjEwNiIgeT0iOTAiIGZpbGw9IiNGRkZGRkYiIHdpZHRoPSIyIiBoZWlnaHQ9IjIiLz48cmVjdCB4PSI3NCIgeT0iNjMiIGZpbGw9IiNGRkZGRkYiIHdpZHRoPSIxIiBoZWlnaHQ9IjEiLz48cmVjdCB4PSIyMyIgeT0iNjYiIGZpbGw9IiNGRkZGRkYiIHdpZHRoPSIxIiBoZWlnaHQ9IjEiLz48cmVjdCB4PSI1MCIgeT0iMTEwIiBmaWxsPSIjRkZGRkZGIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIi8+PHJlY3QgeD0iNjMiIHk9IjEyOCIgZmlsbD0iI0ZGRkZGRiIgd2lkdGg9IjEiIGhlaWdodD0iMSIvPjxyZWN0IHg9IjQ1IiB5PSIxNDkiIGZpbGw9IiNGRkZGRkYiIHdpZHRoPSIxIiBoZWlnaHQ9IjEiLz48cmVjdCB4PSI5MiIgeT0iMTUxIiBmaWxsPSIjRkZGRkZGIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIi8+PHJlY3QgeD0iNTgiIHk9IjgiIGZpbGw9IiNGRkZGRkYiIHdpZHRoPSIxIiBoZWlnaHQ9IjEiLz48cmVjdCB4PSIxNDciIHk9IjMzIiBmaWxsPSIjRkZGRkZGIiB3aWR0aD0iMiIgaGVpZ2h0PSIyIi8+PHJlY3QgeD0iOTEiIHk9IjQzIiBmaWxsPSIjRkZGRkZGIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIi8+PHJlY3QgeD0iMTY5IiB5PSIyOSIgZmlsbD0iI0ZGRkZGRiIgd2lkdGg9IjEiIGhlaWdodD0iMSIvPjxyZWN0IHg9IjE4MiIgeT0iMTkiIGZpbGw9IiNGRkZGRkYiIHdpZHRoPSIxIiBoZWlnaHQ9IjEiLz48cmVjdCB4PSIxNjEiIHk9IjU5IiBmaWxsPSIjRkZGRkZGIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIi8+PHJlY3QgeD0iMTM4IiB5PSI5NSIgZmlsbD0iI0ZGRkZGRiIgd2lkdGg9IjEiIGhlaWdodD0iMSIvPjxyZWN0IHg9IjE5OSIgeT0iNzEiIGZpbGw9IiNGRkZGRkYiIHdpZHRoPSIzIiBoZWlnaHQ9IjMiLz48cmVjdCB4PSIyMTMiIHk9IjE1MyIgZmlsbD0iI0ZGRkZGRiIgd2lkdGg9IjIiIGhlaWdodD0iMiIvPjxyZWN0IHg9IjEyOCIgeT0iMTYzIiBmaWxsPSIjRkZGRkZGIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIi8+PHJlY3QgeD0iMjA1IiB5PSIxNzQiIGZpbGw9IiNGRkZGRkYiIHdpZHRoPSIxIiBoZWlnaHQ9IjEiLz48cmVjdCB4PSIxNTIiIHk9IjIwMCIgZmlsbD0iI0ZGRkZGRiIgd2lkdGg9IjEiIGhlaWdodD0iMSIvPjxyZWN0IHg9IjUyIiB5PSIyMTEiIGZpbGw9IiNGRkZGRkYiIHdpZHRoPSIyIiBoZWlnaHQ9IjIiLz48cmVjdCB5PSIxOTEiIGZpbGw9IiNGRkZGRkYiIHdpZHRoPSIxIiBoZWlnaHQ9IjEiLz48cmVjdCB4PSIxMTAiIHk9IjE4NCIgZmlsbD0iI0ZGRkZGRiIgd2lkdGg9IjEiIGhlaWdodD0iMSIvPjwvc3ZnPg==)",

  // "#starfield": {
  //   position: "fixed",
  //   top: "0",
  //   left: "0",
  //   right: "0",
  //   bottom: "0",
  //   width: "100vw",
  //   height: "100vh",
  //   background: "darken(#22313f, 5%)",
  //   zIndex: "-5",
  // },
  // p: {
  //   position: "fixed",
  //   zIndex: "2",
  //   top: "50%",
  //   left: "0",
  //   right: "0",
  //   textAlign: "center",
  //   transform: "translateY(-50%)",
  //   fontSize: "40px",
  //   fontWeight: "900",
  //   color: "white",
  //   textShadow: "0 0 50px black",
  //   textTransform: "uppercase",
  //   fontFamily: "'Roboto','Helvetica','Arial',sans-serif",
  //   letterSpacing: "5px",
  //   "> span": {
  //     display: "block",
  //     fontSize: "12px",
  //     color: "#bdc3c7",
  //     marginTop: "30px",
  //     fontWeight: "100",
  //     textShadow: "0 0 50px black",
  //     letterSpacing: "3px",
  //     "> a": {
  //       fontWeight: "700",
  //       textDecoration: "none",
  //       color: "#d64541",
  //       paddingBottom: "2px",
  //       borderBottom: "0px solid #d64541",
  //       transition: "0.5s",
  //     },
  //     "> a:hover": {
  //       paddingBottom: "5px",
  //       borderBottom: "2px solid #d64541",
  //     },
  //   },
  // },
});

const BackgroundElementWrapper = styled("div")({
  position: "fixed",
  width: "100%",
  height: "100%",
  zIndex: 1,
  top: 0,
  left: 0,
  overflow: "hidden",
  ".st0": {
    clipPath: "url(#SVGID_00000069358781714097307270000011167628922718028479_",
    fill: "url(#SVGID_00000062908172388032337340000002838961251132092562_)",
  },
  st1: {
    clipPath: "url(#SVGID_00000069358781714097307270000011167628922718028479_)",
  },
  st2: {
    filter: "url(#Adobe_OpacityMaskFilter)",
  },
  ".st3": {
    mask: "url(#SVGID_00000106136712691738400910000001410175658615626369_)",
    fill: "url(#SVGID_00000140693229728757374690000003881397427174932146_)",
  },
});

const MovingStars = () => {
  useEffect(() => {
    // var colours = [
    //   "#f5d76e",
    //   "#f7ca18",
    //   "#f4d03f",
    //   "#ececec",
    //   "#ecf0f1",
    //   "#a2ded0",
    // ];
    // var stars = [];
    // var initialise = function () {
    //   var canvas = document.getElementById("starfield");
    //   (canvas.width = window.innerWidth), (canvas.height = window.innerHeight);
    //   for (var i = 0, l = 100; i < l; i++) {
    //     var radius = Math.random() * 1.5;
    //     stars.push({
    //       radius: radius,
    //       x: Math.random() * canvas.width,
    //       y: Math.random() * canvas.height,
    //       colour: colours[parseInt(Math.random() * 4)],
    //       blur: Math.random() * 10,
    //       pulse: true,
    //       threshold: radius * 1.25,
    //     });
    //   }
    //   window.requestAnimationFrame(draw);
    // };
    // var generatePulseVariance = function (star, canvas) {
    //   if (star.pulse) {
    //     star.radius += 0.075;
    //     star.pulse = star.radius <= star.threshold;
    //   } else {
    //     if (star.radius >= 1) star.radius -= 0.075;
    //     star.pulse = star.radius <= 1;
    //   }
    //   if (star.x < canvas.width) star.x += 0.35;
    //   else star.x = 0;
    //   return star;
    // };
    // var draw = function () {
    //   var canvas = document.getElementById("starfield"),
    //     context = canvas.getContext("2d");
    //   context.clearRect(0, 0, canvas.width, canvas.height);
    //   for (var i = 0, l = stars.length; i < l; i++) {
    //     var star = stars[i];
    //     star = generatePulseVariance(star, canvas);
    //     context.beginPath();
    //     context.arc(star.x, star.y, star.radius, 0, 2 * Math.PI, false);
    //     context.fillStyle = star.colour;
    //     context.shadowColor = star.colour;
    //     context.shadowBlur = star.blur;
    //     context.fill();
    //   }
    //   window.requestAnimationFrame(draw);
    // };
    // initialise();
  }, []);

  const SpaceBackground = styled("div")({
    position: "fixed",
    width: "100%",
    top: 0,
    left: 0,
    height: "100%",
    zIndex: -3,
    background: `url('${FogImage}') repeat`,
    backgroundSize: "cover",

    // animation: css`
    //   ${Spin} 900s linear infinite
    // `,
  });

  const FogWrapper = styled("div")({
    position: "fixed",
    width: "100%",
    height: "100%",
    zIndex: 1,
  });

  return (
    <>
      <SpaceBackground />
      {/* <BackgroundElement /> */}
      <MovingStarsWrapper />
    </>
  );
};

export default MovingStars;
